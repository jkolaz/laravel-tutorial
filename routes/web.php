<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Route::prefix('linkedin')->group(function () {
//    Route::get('/login', 'LinkedinController@login');
//});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('/indeed/code', 'IndeedController@indeedCallback');


Route::get('/indeed', 'IndeedController@index')->name('indeed');
Route::get('/indeed/code', 'IndeedController@indeedCallback')->name('indeed.callback');
Route::get('/indeed/response', 'IndeedController@callback')->name('indeed.response');
