<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class IndeedController extends Controller
{
    public function index() {

        $urlIndeed = env( 'INDEED_API_URL');
        $urlIndeed .= '?client_id=' . env( 'INDEED_TOKEN_ID' );
        $urlIndeed .= '&redirect_uri=' . urlencode( env( 'INDEED_CALLBACK_URL' ) );
        $urlIndeed .= '&response_type=code';
        dd( $urlIndeed );
    }

    public function indeedCallback( Request $request ) {
        $code = $request->code;
        $endpoint =  env( 'INDEED_OAUTH_URL' );

        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', $endpoint, [
            'headers' => [
                'Content-Type' => 'application/x-www-form-urlencoded',
                'Accept'     => 'application/json',
            ],
            'query' => [
                'code' => $code,
                'client_id' => env( 'INDEED_TOKEN_ID' ),
                'client_secret' => env( 'INDEED_API_KEY_SECRET' ),
                'redirect_uri' => env( 'INDEED_CALLBACK_URL' ),
                'grant_type' => 'authorization_code'
            ],
        ]);

        $body = $response->getBody();
        $stringBody = (string) $body;

        dd( $response, $response->getStatusCode(), $stringBody );
    }

    public function callback( Request $request ) {
        dd( $request->all() );
    }
}
