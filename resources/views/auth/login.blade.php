@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0 mt-3">
                            <div class="col-md-8 offset-md-4">
                                <a href="{{ url('/login/linkedin') }}" class="btn btn-success">
                                    {{ __('Login with LinkedIn') }}
                                </a>
                            </div>
                        </div>
                        <div class="form-group row mb-0 mt-3">
                            <div class="col-md-8 offset-md-4">
                                <a href="{{ route('indeed') }}" class="btn btn-info">
                                    {{ __('Login with Indeed') }}
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var popup;
    var breezyUrl="https://app.breezy.hr";
    var positionId="b90c730c6d0b";
    https://secure.indeed.com/account/oauth?response_type=code&state=3698269a8e09&client_id=418e94de0fceab5b4d08764cc0d9f7cbc63be4bf20919138b3025c8d2b0182bd&redirect_uri=https%3A%2F%2Fapp.breezy.hr%2Fapi%2Fapply%2Findeed%2Fcallback
    function oAuthListener (msg) {
        if (msg.data.token) {
            window.location.href = window.location.href + '/apply?token=' + msg.data.token;
            popup.close()
        } else {
            window.location.href = window.location.href + '/apply';
            popup.close()
        }
        window.removeEventListener('message', oAuthListener)
    }
    function openPopup (e) {
        e.preventDefault();

        window.addEventListener('message', oAuthListener)

        var params = popupParams();
        popup = window.open(
            breezyUrl + '/api/apply/indeed?position_id=' + positionId,
            'IndeedPopup',
            params
        );
    }
</script>
@endsection
